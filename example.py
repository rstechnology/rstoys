
from rstoys import touchy
from rstoys import realtime


def control(elapsed, dt):
    (x, y) = touchy.controller.getStick()
    print("elapsed=%08.3fs | dt=%06.3fms | X=%6.3f | Y=%6.3f" % (elapsed, dt * 1000, x, y))


touchy.start_server(port=5002)

realtime.loop(control)

print("Finished loop")

touchy.stop_server()

print("Finished all")
