# rstoys

Cross platform python lightweight library to help creating interactive real-time, remote controlled toys (typically based on Raspberry Pi). Library provides set of independent reusable modules described below.

Source code can be found at [https://bitbucket.org/rstechnology/rstoys/src/master/](https://bitbucket.org/rstechnology/rstoys/src/master/)

Please use pip to install this package:
```sh
pip install rstoys
```

## rstoys.realtime

realtime module provides some functions related to realtime main loop processing functionality.
All time or interval values are expressed in `seconds`.

### `realtime.loop(callback, updateInterval = 0.020)`

this function enters and endless loop that calls your `callback` function with two parameters `(lapsedTime, deltaTime)` at specified updateInterval.

- `elapsedTime` - is an absolute elapsed time in seconds starting with 0 at the beginning
- `deltaTime` - is a delta time in seconds since last execution of your callback function, can be used to scale your robot actions to make it more cpu load independent.

It is an equivalent of:
```python
while True:
    callback(elapsedTime, deltaTime)
    sleep(updateInterval)
```
Using the realtime.loop() has an advantage that it dynamically adjusts the sleep time to compensate for the time spent in your callback, trying to provide as stable callback calling interval as possible.

So far the only way to exit this loop is by pressing `CTRL+C`.

Example usage:
```python
from rstoys import realtime

def update(elapsedTime, deltaTime):
    print("elapsed=%08.3fs | dt=%06.3fms" % (elapsedTime, deltaTime * 1000))

# initialize my hardware here

# enter endless loop that calls our update function every 100ms
realtime.loop(update, 0.1)
```

## rstoys.touchy

This module provides a mobile friendly web interface with basic touch stick controller for (x, y) values that can be easily read in your python code for controlling your robot.

### `touchy.start_server(port=80)`
starts simple flask webserver instance on a given port that is running in a separate thread this webserver can be reached:

Locally `http://127.0.0.1:<port>/` or remotely by using your IP address of the RPi instead of 127.0.0.1

### `touchy.stop_server()`
stops touchy webserver and terminates its thread.

### `(x, y) = touchy.controller.getStick()`
This function returns (x, y) tupple representing touch stick controller position.
values are in range of `[-1...0...1]`.

Example code:

```python
from rstoys import touchy
from rstoys import realtime

def update(elapsed, dt):
    (x, y) = touchy.controller.getStick()
    print("elapsed=%08.3fs | dt=%06.3fms | X=%6.3f | Y=%6.3f" % (elapsed, dt * 1000, x, y))

touchy.start_server(port=5000)
realtime.loop(update)
touchy.stop_server()
```
... then open `http://<YOUR-IP-ADDRESS>:5000/` in your mobile browser and move the on-screen stick to see the changes.

### Development

Want to contribute? Just send me an e-mail: `rstechnology@gmail.com`

## License

MIT

Use at your own risk and have fun.
